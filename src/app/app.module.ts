import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {RouterModule} from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { HomeComponent } from './misc/home.component';
import { NotFoundComponent } from './misc/not-found.component';
import {ConfigModule} from './config/config.module';
import {HttpClientModule} from '@angular/common/http';
import {AuthGuard} from './user/auth.guard';
import { WelcomeComponent } from './misc/welcome.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NotFoundComponent,
    WelcomeComponent,
  ],
  imports: [
    BrowserModule,
    NgbModule,
    ConfigModule,
    HttpClientModule,
    RouterModule.forRoot([
      {path: 'welcome', component: WelcomeComponent},
      {path: 'home', component: HomeComponent, canActivate: [AuthGuard]},
      {path: 'user', loadChildren: './user/user.module#UserModule'},
      {path: 'role', loadChildren: './role/role.module#RoleModule', canActivate: [AuthGuard]},
      {path: 'task', loadChildren: './task/task.module#TaskModule', canActivate: [AuthGuard]},
      {path: '', redirectTo: 'home', pathMatch: 'full'},
      {path: '**', component: NotFoundComponent},
    ]),
    BrowserAnimationsModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
