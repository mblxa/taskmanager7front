import {Component} from '@angular/core';
import {UserService} from './user/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'App';

  constructor(
    private userService: UserService,
    private router: Router,
  ) {}

  authorized(): boolean {
    return this.userService.isAuthorized();
  }

  logout() {
    this.userService.clearToken();
    this.router.navigateByUrl('/welcome');
    return false;
  }
}
