import {Inject, Injectable} from '@angular/core';
import {APP_CONFIG, AppConfig} from '../config/config.module';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Observable, of} from 'rxjs/index';
import { catchError, map, tap } from 'rxjs/operators';

export enum AuthResponse {
  Success = 200,
  Unathorized = 401
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private token: string = null;
  constructor(
    @Inject(APP_CONFIG) private config: AppConfig,
    private http: HttpClient
  ) {
    this.token = localStorage.getItem('token');
  }

  getToken(): string {
    return this.token;
  }

  isAuthorized(): boolean {
    return this.getToken() !== null;
  }

  clearToken(): void {
    this.token = null;
    localStorage.removeItem('token');
  }

  auth(credentials: AuthCredentials) {
    const body = JSON.stringify(credentials);
    const headers = new HttpHeaders({'Content-Type': 'application/json'});

    return this.http.post(this.config.apiEndpoint + '/user/auth', body, {headers: headers})
      .pipe(
        map(this.authSuccess, this),
        catchError(this.handleError('auth', []))
      );
  }

  register(credentials: AuthCredentials) {
    const body = JSON.stringify(credentials);
    const headers = new HttpHeaders({'Content-Type': 'application/json'});

    return this.http.post(this.config.apiEndpoint + '/user', body, {headers: headers})
      .pipe(
        map(this.authSuccess, this),
        catchError(this.handleError('auth', []))
      );
  }

  authSuccess(res: ResponseToken) {
    console.log(res);
    this.token = res.token;
    localStorage.setItem('token', res.token);
    return AuthResponse.Success;
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(error.status as T);
    };
  }

}

export interface AuthCredentials {
  email: string;
  password: string;
}

export interface ResponseToken {
  token: string;
}
