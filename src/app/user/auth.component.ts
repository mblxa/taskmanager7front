import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthCredentials, AuthResponse, UserService} from './user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  public pageTitle = 'Login';
  public userEmail: string;
  public userPassword: string;
  public invalidCredentials = false;

  public authForm: FormGroup;

  constructor(
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit() {
    const email = new FormControl(this.userEmail,
      [
        Validators.required,
      ]);
    const password = new FormControl(this.userPassword,
      [
        Validators.required
      ]);

    this.authForm = new FormGroup({
      email: email,
      password: password
    });
  }

  validateEmail() {
    if (this.invalidCredentials) {
      return false;
    }
    return (this.authForm.get('email').valid || !this.authForm.get('email').dirty);
  }

  validatePassword() {
    return (this.authForm.get('password').valid || !this.authForm.get('password').dirty);
  }

  auth(credentials: AuthCredentials) {
    if (this.authForm.valid) {
      this.userService.auth(credentials).subscribe(
        data => {
          switch (data) {
            case AuthResponse.Success:
              this.invalidCredentials = false;
              this.router.navigate(['/home']);
              break;
            default:
              this.invalidCredentials = true;
              break;
          }
          return;
        },
        err => {
          console.log(err);
        }
      );
    } else {
      this.authForm.get('email').markAsDirty();
      this.authForm.get('password').markAsDirty();
    }
  }
}
