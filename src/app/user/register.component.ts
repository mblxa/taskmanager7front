import { Component, OnInit } from '@angular/core';
import {AuthCredentials, AuthResponse, UserService} from "./user.service";
import {Router} from "@angular/router";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  title = 'Register';
  authForm: FormGroup;

  constructor(
    private userService: UserService,
    private router: Router,
  ) { }

  ngOnInit() {
    const email = new FormControl(undefined,
      [
        Validators.required,
      ]);
    const password = new FormControl(undefined,
      [
        Validators.required
      ]);
    const repeat = new FormControl(undefined,
      [
        Validators.required
      ]);

    this.authForm = new FormGroup({
      email: email,
      password: password,
      repeat: repeat,
    });
  }

  validateEmail() {
    return (this.authForm.get('email').valid || !this.authForm.get('email').dirty);
  }

  validatePassword() {
    return (this.authForm.get('password').valid || !this.authForm.get('password').dirty);
  }

  validateRepeat() {
    return (this.authForm.get('repeat').valid || !this.authForm.get('repeat').dirty);
  }

  validateMatch() {
    if (!this.authForm.get('repeat').dirty) {
      return true;
    }
    return this.authForm.get('password').value === this.authForm.get('repeat').value
  }

  register(credentials: AuthCredentials) {
    if (this.authForm.valid && this.validateMatch()) {
      this.userService.register(credentials).subscribe(
        data => {
          switch (data) {
            case AuthResponse.Success:
              // this.router.navigate(['/home']);
              break;
            default:
              break;
          }
          return;
        },
        err => {
          console.log(err);
        }
      );
    } else {
      this.authForm.get('email').markAsDirty();
      this.authForm.get('password').markAsDirty();
    }
  }

}
