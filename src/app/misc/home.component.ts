import { Component, OnInit } from '@angular/core';
import {ITask, ITaskByDate} from '../task/task';
import {SORT_ORDER, TASK_STATUS, TaskService} from '../task/task.service';
import {RoleService} from '../role/role.service';
import {IRole} from '../role/role';
import {timer} from 'rxjs/index';

const MS_PER_MINUTE = 60000;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  tasks: ITaskByDate[];
  roles: IRole[];

  constructor(
    private taskService: TaskService,
    private roleService: RoleService,
  ) { }

  ngOnInit() {
    this.fetchTasks();
    this.roleService.getRoles().subscribe(
      roles => {
        this.roles = roles;
      },
      err => {
        console.log(err);
      }
    );

    const timerVar = timer(new Date(), 30000).pipe();
    timerVar.subscribe(t => {
      if (this.tasks) {
        this.checkTaskStart();
      }
    });
  }

  dateNow(): Date {
    return new Date();
  }

  fetchTasks(): void {
    this.taskService.getTasks(SORT_ORDER.byDate, undefined).subscribe(
      tasks => {
        this.tasks = this.taskService.groupByDate(tasks);
      },
      err => {
        console.log(err);
      }
    );
  }

  changeStatus(taskID: number, status: number): boolean {
    if (!confirm('Complete?')) {
      return false;
    }

    let newStatus = TASK_STATUS.new;
    if (status == parseInt(TASK_STATUS.new)) {
      newStatus = TASK_STATUS.complete;
    }

    this.taskService.changeStatus(taskID, parseInt(newStatus)).subscribe(
      data => {
        this.fetchTasks();
      },
      err => {
        console.log(err);
      }
    );

    return false;
  }

  checkTaskStart(): boolean {
    for (const taskGroup of this.tasks) {
      for (const task of taskGroup.tasks) {
      if (this.taskSoon(task)) {
        this.fetchTasks();
        return true;
      }
      }
    }
    return false;
  }

  taskSoon(task: ITask): boolean {
    const compareWithDate = new Date(task.Date.getTime() - 5 * MS_PER_MINUTE);
    return this.dateNow() > compareWithDate;
  }

  taskStarted(task: ITask): boolean {
    return this.dateNow() > task.Date;
  }

  taskEnded(task: ITask): boolean {
    const compareWith = new Date(task.Date.getTime() + task.Duration * MS_PER_MINUTE);
    return this.dateNow() > compareWith;
  }

  postponeTask(task: ITask, minutes: number) {
    task.Date = new Date(task.Date.getTime() + minutes * MS_PER_MINUTE);
    this.taskService.createTask(task).subscribe(
      ok => {
        this.fetchTasks();
      }
    );
  }
}
