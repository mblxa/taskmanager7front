import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { TaskListComponent } from './task-list.component';
import { TaskCreateComponent } from './task-create.component';
import {CalendarModule} from 'primeng/calendar';
import {SliderModule} from 'primeng/slider';
import {ListboxModule} from 'primeng/listbox';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CalendarModule,
    SliderModule,
    ListboxModule,
    RouterModule.forChild([
      {path: 'list', component: TaskListComponent},
      {path: 'create', component: TaskCreateComponent},
      {path: 'update/:id', component: TaskCreateComponent},
    ]),
  ],
  declarations: [TaskListComponent, TaskCreateComponent],
  exports: [
    RouterModule
  ]
})
export class TaskModule { }
