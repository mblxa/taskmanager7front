import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {UserService} from '../user/user.service';
import {APP_CONFIG, AppConfig} from '../config/config.module';
import {merge, Observable, of} from 'rxjs/index';
import {catchError, map} from 'rxjs/operators';
import {ITask} from './task';
import {Router} from '@angular/router';
import {T} from "@angular/core/src/render3";
import {delay} from "rxjs/internal/operators";

export enum SORT_ORDER {
  byDate = 'byDate'
}

export enum TASK_STATUS{
  new = '1',
  complete = '2',
  all = 'all'
}

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  private apiEndpoint = this.config.apiEndpoint + '/tasks';

  constructor(
    @Inject(APP_CONFIG) private config: AppConfig,
    private http: HttpClient,
    private userService: UserService,
    private httpRouter: Router
  ) { }

  private getHeader(): HttpHeaders {
    return new HttpHeaders({'token': this.userService.getToken()});
  }

  getTasks(sortOrder?: SORT_ORDER, status?: TASK_STATUS): Observable<ITask[]> {
    return this.getFromServer(sortOrder, status);
  }

  getFromServer(sortOrder?: SORT_ORDER, status?: TASK_STATUS): Observable<ITask[]> {
    let url = this.apiEndpoint;
    if (sortOrder) {
      url += '/' + sortOrder;
    }
    if (status) {
      url += '?status=' + status;
    }

    return this.http.get<ITask[]>(url, {headers: this.getHeader()})
      .pipe(
        map(data => {
          for (const i in data) {
            if (data[i]) {
              data[i].Date = new Date(data[i].Date);
            }
          }

          return data;
        }),
        catchError(this.handleError('getTasks', []))
      );
  }

  getTask(id: number): Observable<ITask> {
    return this.http.get<ITask>(this.apiEndpoint + `/${id}`, {headers: this.getHeader()})
      .pipe(
        map(data => {
          data.Date = new Date(data.Date);
          return data;
        }),
        catchError(this.handleError('getTask', undefined))
      );
  }

  createTask(data: ITask): Observable<any> {
    data.RoleID = +data.RoleID;
    data.Status = +data.Status;
    const body = JSON.stringify(data);
    return this.http.post(this.apiEndpoint, body, {headers: this.getHeader()})
      .pipe(
        catchError(this.handleError('createTask', []))
      );
  }

  deleteTask(id: number): Observable<boolean> {
    return this.http.delete<boolean>(this.apiEndpoint + `/${id}`, {headers : this.getHeader()})
      .pipe(
        map(_ => {
          return true;
        }),
        catchError(this.handleError('deleteTask', false))
      );
  }

  changeStatus(taskID: number, status: number): Observable<any> {
    const body = JSON.stringify({
      Status: status
    });
    return this.http.post(this.apiEndpoint + '/change-status/' + taskID, body, {headers: this.getHeader()})
      .pipe(
        catchError(this.handleError('updateTask', []))
      );
  }

  groupByDate(tasks: ITask[]) {
    const intermediate = [];
    const result = [];

    for (const task of tasks) {
      const date = task.Date.getFullYear() + '-' + (task.Date.getMonth() + 1) + '-' + task.Date.getDate();
      if (!intermediate[date]) {
        intermediate[date] = {
          'date': new Date(date),
          'tasks': []
        };
      }
      intermediate[date].tasks.push(task);
    }

    for (const i in intermediate) {
      if (intermediate[i]) {
        result.push(intermediate[i]);
      }
    }

    return result;
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      this.userService.clearToken();
      this.httpRouter.navigateByUrl('/welcome');

      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
