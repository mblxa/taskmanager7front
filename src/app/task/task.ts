export interface ITask {
  ID: number;
  Name: string;
  Description: string;
  Status: number;
  Date: Date;
  Duration: number;
  RoleID: number;
}

export interface ITaskByDate {
  date: Date;
  tasks: ITask[];
}
