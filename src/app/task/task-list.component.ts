import { Component, OnInit } from '@angular/core';
import {TASK_STATUS, TaskService} from './task.service';
import {ITask} from './task';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {
  public title = 'Tasks';
  public taskList: ITask[];
  public taskStatusShow: TASK_STATUS = TASK_STATUS.new;

  constructor(
    private taskService: TaskService,
  ) { }

  ngOnInit() {
    this.fetchTasks();
  }

  fetchTasks(): void {
    this.taskService.getTasks(null, this.taskStatusShow).subscribe(
      tasks => {
        this.taskList = tasks;
      }, err => {
        console.log(err);
      }
    );
  }

  deleteTask(id: number, name: string) {
    if (confirm(`Are you sure you want to delete "${name}"?`)) {
      this.taskService.deleteTask(id).subscribe(
        data => {
          if (data === true) {
            this.fetchTasks();
          }
        },
        err => {
          console.log(err);
        }
      );
    }
    return false;
  }

  toggleTaskFilter() {
    if (this.taskStatusShow === TASK_STATUS.new) {
      this.taskStatusShow = TASK_STATUS.all;
    } else if (this.taskStatusShow === TASK_STATUS.all) {
      this.taskStatusShow = TASK_STATUS.complete;
    } else {
      this.taskStatusShow = TASK_STATUS.new;
    }

    this.fetchTasks();
    return false;
  }
}
