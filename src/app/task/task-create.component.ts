import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {RoleService} from '../role/role.service';
import {validate} from 'codelyzer/walkerFactory/walkerFn';
import {IRole} from '../role/role';
import {ITask} from './task';
import {TaskService} from './task.service';

@Component({
  selector: 'app-task-create',
  templateUrl: './task-create.component.html',
  styleUrls: ['./task-create.component.css']
})
export class TaskCreateComponent implements OnInit {
  public title = 'Create new Task';
  public buttonSubmit = 'Create';
  public form: FormGroup;
  public roles: IRole[];
  public selectedRole: number;
  public en: any;
  public redirectHome: boolean = false;


  constructor(
    private taskService: TaskService,
    private roleService: RoleService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    const home = this.activatedRoute.snapshot.queryParamMap.get('home');
    if (home) {
      this.redirectHome = true;
    }

    this.en = {
      firstDayOfWeek: 1,
      dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
      dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
      dayNamesMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
      monthNames: [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October',
        'November', 'December' ],
      monthNamesShort: [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ],
      today: 'Today',
      clear: 'Clear'
    };

    const taskID = new FormControl(undefined);
    const taskName = new FormControl(undefined,
      [
        Validators.required,
      ]);
    const taskDate = new FormControl(new Date(),
      [
        Validators.required,
      ]);
    const taskDuration = new FormControl(10,
      [
        Validators.required,
      ]);
    const taskRole = new FormControl(undefined,
      [
        Validators.required,
      ]);
    const taskStatus = new FormControl(1,
      [
        Validators.required,
      ]);
    const taskDescription = new FormControl();
    this.form = new FormGroup({
      ID: taskID,
      Name: taskName,
      Date: taskDate,
      Duration: taskDuration,
      RoleID: taskRole,
      Description: taskDescription,
      Status: taskStatus
    });

    const id = +this.activatedRoute.snapshot.paramMap.get('id');
    if (id) {
      this.buttonSubmit = 'Update';
      this.taskService.getTask(id).subscribe(
        data => {
          this.form.setValue({
            ID: data.ID,
            Name: data.Name,
            Duration: data.Duration,
            Date: data.Date,
            RoleID: data.RoleID,
            Description: data.Description,
            Status: data.Status
          });
          this.selectedRole = data.RoleID;
        },
        err => {
          console.log(err);
        }
      );
    }

    this.roleService.getRoles().subscribe(
      roles => {
        this.roles = roles;
      },
      err => {
        console.log(err);
      }
    );
  }

  validateName(): boolean {
    return (this.form.get('Name').valid || !this.form.get('Name').dirty);
  }

  submitForm(formData: ITask) {
    if (this.form.valid) {
      this.taskService.createTask(formData)
        .subscribe(
          data => {
            this.redirectBack();
          },
          error => {
            console.error(error);
          }
        );
    } else {
      this.form.get('Name').markAsDirty();
    }
  }

  back(): void {
    this.redirectBack();
  }

  redirectBack(): void {
    if (this.redirectHome) {
      this.router.navigateByUrl('/home');
    } else {
      this.router.navigateByUrl('/task/list');
    }
  }
}
