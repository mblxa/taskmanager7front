import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {IRole} from './role';
import {RoleService} from './role.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-role-create',
  templateUrl: './role-create.component.html',
  styleUrls: ['./role-create.component.css']
})
export class RoleCreateComponent implements OnInit {
  title = 'Create new Role';
  buttonSubmit = 'Create';
  roleForm: FormGroup;
  color: string;

  constructor(
    private roleService: RoleService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    const roleId = +this.activatedRoute.snapshot.paramMap.get('id');
    if (roleId) {
      this.buttonSubmit = 'Update';
      this.roleService.getRole(roleId).subscribe(
        data => {
          this.roleForm.setValue({
            ID: data.ID,
            Name: data.Name,
            Color: data.Color
          });
          this.color = data.Color;
        },
        err => {
          console.log(err);
        }
      );
    }
    const roleID = new FormControl(undefined);
    const roleColor = new FormControl(undefined, [
    ]);
    const roleName = new FormControl(undefined,
      [
        Validators.required,
      ]);
    this.roleForm = new FormGroup({
      ID: roleID,
      Name: roleName,
      Color: roleColor
    });
  }

  validateName(): boolean {
    return (this.roleForm.get('Name').valid || !this.roleForm.get('Name').dirty);
  }

  submitForm(formData: IRole) {
    formData.Color = this.color;
    console.log(formData);
    if (this.roleForm.valid) {
      this.roleService.createRole(formData)
        .subscribe(
          data => {
            this.router.navigateByUrl('/role/list');
          },
          error => {
            console.error(error);
          }
        );
    } else {
      this.roleForm.get('Name').markAsDirty();
    }
  }

  back(): void {
    this.router.navigateByUrl('/role/list');
  }

}
