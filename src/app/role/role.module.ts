import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ColorPickerModule} from 'ngx-color-picker';
import {RoleListComponent} from './role-list.component';
import {RoleCreateComponent} from './role-create.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ColorPickerModule,
    RouterModule.forChild([
      {path: 'list', component: RoleListComponent},
      {path: 'create', component: RoleCreateComponent},
      {path: 'update/:id', component: RoleCreateComponent},
    ]),
  ],
  declarations: [RoleListComponent, RoleCreateComponent],
  exports: [
    RouterModule
  ]
})
export class RoleModule { }
