import {Inject, Injectable} from '@angular/core';
import {merge, Observable, of} from 'rxjs/index';
import {IRole} from './role';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {APP_CONFIG, AppConfig} from '../config/config.module';
import {catchError, map} from 'rxjs/operators';
import {UserService} from '../user/user.service';
import {Router} from '@angular/router';
import {ITask} from "../task/task";

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  private apiEndpoint = this.config.apiEndpoint + '/roles';

  constructor(
    @Inject(APP_CONFIG) private config: AppConfig,
    private http: HttpClient,
    private userService: UserService,
    private httpRouter: Router
  ) { }

  private getHeader(): HttpHeaders {
    return new HttpHeaders({'token': this.userService.getToken()});
  }

  getRoles(): Observable<IRole[]> {
    const sources = merge(
      this.getFromServer(),
      this.getFromStorage()
    );

    return sources;
  }

  getRole(id: number): Observable<IRole> {
    return this.http.get<IRole>(this.apiEndpoint + `/${id}`, {headers: this.getHeader()})
      .pipe(
        catchError(this.handleError('getRole', undefined))
      );
  }

  createRole(data: IRole): Observable<any> {
    const body = JSON.stringify(data);
    return this.http.post<IRole>(this.apiEndpoint, body, {headers: this.getHeader()})
      .pipe(
        catchError(this.handleError('createRole', []))
      );
  }

  deleteRole(id: number): Observable<boolean> {
    return this.http.delete<boolean>(this.apiEndpoint + `/${id}`, {headers : this.getHeader()})
      .pipe(
        map(_ => {
          return true;
        }),
        catchError(this.handleError('deleteRole', false))
      );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      this.userService.clearToken();
      this.httpRouter.navigateByUrl('/home');

      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  getFromServer(): Observable<IRole[]> {
    return this.http.get<IRole[]>(this.apiEndpoint, {headers: this.getHeader()})
      .pipe(
        map(data => {
          this.saveToStorage(data);
          return data
        }),
        catchError(this.handleError('getRoles', []))
      );
  }

  getFromStorage(): Observable<IRole[]> {
    let roleString = localStorage.getItem('roles');
    let roles = [];
    if (roleString !== null) {
      roles = JSON.parse(roleString);
    }

    return new Observable(function (observer) {
      observer.next(roles);
      observer.complete();
    });
  }

  saveToStorage(tasks: IRole[]) {
    localStorage.setItem('roles', JSON.stringify(tasks));
  }
}
