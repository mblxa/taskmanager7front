export interface IRole {
  ID: number;
  CreatedAt: Date;
  UpdatedAt: Date;
  DeletedAt: Date;
  Name: string;
  Color: string;
  UserID: number;
}
