import { Component, OnInit } from '@angular/core';
import {RoleService} from './role.service';
import {IRole} from './role';

@Component({
  selector: 'app-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.css']
})
export class RoleListComponent implements OnInit {
  public title = 'Roles';

  public roleList: IRole[];

  constructor(
    private roleService: RoleService
  ) { }

  ngOnInit() {
    this.fetchRoles();
  }

  fetchRoles() {
    this.roleService.getRoles().subscribe(
      data => {
        this.roleList = data;
      },
      error => {
        console.log(error);
      }
    );
  }

  deleteRole(roleId: number, roleName: string) {
    if (confirm(`Are you sure you want to delete role "${roleName}"?`)) {
      this.roleService.deleteRole(roleId).subscribe(
        data => {
          if (data === true) {
            this.fetchRoles();
          }
        },
        err => {
          console.log(err);
        }
      );
    }
    return false;
  }

}
